# Scottish Geology Trust Geosites Project Website

A website to deliver information submitted to the database of Geosites as part of the SGT's Geosites project.

[Read more about the project](https://www.scottishgeologytrust.org/geology/geosites/) | [Go to the production instance](https://geosites.scottishgeologytrust.org)

## Tech

- [Django](https://www.djangoproject.com/) web-app
- [PostgreSQL](https://www.postgresql.org/) database (with [PostGIS](https://postgis.net/) extension)
- [Leaflet](https://leafletjs.com/) map library

## Running locally

1. Install prerequisites: Python, Git, PostgreSQL, PostGIS (and create a database for the app to use with PostGIS enabled), anything else...?

2. Clone the repository:

        $ git clone https://codeberg.org/dburgess/sgt-geosites.git
        $ cd sgt-geosites

3. Set up the Python environment:

        $ python -m venv venv
        $ source venv/bin/activate
        
    (on Linux, might be different on other systems?)

4. Install dependencies:

        $ pip install -r requirements.txt

5. Apply the (hacky) patch to django-leaflet to get it to work with EPSG:27700: follow instructions in `geosites/geosites_app/django_leaflet_crs.patch`

6. Configure the local environment: follow instructions in `geosites/.env.example`. Provide the credentials for the local Postgres database, generate a secret key and provide the API key for the Ordnance Survey basemap.

7. Do the database migrations (this will create all the tables needed in the database):

        $ python manage.py migrate

8. Copy static files into a directory so Django can serve them:

        $ python manage.py collectstatic

9. Cross your fingers and run the server!

        $ python manage.py runserver

10. The app should be running on [http://127.0.0.1:8000](http://127.0.0.1:8000).