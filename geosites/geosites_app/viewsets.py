from rest_framework import viewsets
from rest_framework_gis import filters
from rest_framework.exceptions import ParseError

from geosites_app.models import Geosite, SitePhotograph
from geosites_app.serializers import GeositeSerializerExtent, GeositeSerializerPolygons, GeositeSerializerPoints, SitePhotographSerializer

class GeositeViewset(viewsets.ReadOnlyModelViewSet):
    bbox_filter_field = "geom"
    bbox_filter_include_overlapping = True
    filter_backends = (filters.InBBoxFilter,)

    def get_queryset(self):
        # Only serve Scottish geosites
        queryset = Geosite.objects.filter(gcrsite__agency="SNH")
        queryset = queryset.filter(visible=True)

        blocks = self.request.query_params.get("blocks")
        geom_type = self.request.query_params.get("geom")

        if blocks:
            blocks_list = blocks.upper().split(",")
            queryset = queryset.filter(gcrsite__block_name__parent__in=blocks_list)

        if geom_type:
            if geom_type.lower() == "point":
                self.bbox_filter_field = "geom_point"
                self.serializer_class = GeositeSerializerPoints
            elif geom_type.lower() == "polygon":
                self.bbox_filter_field = "geom"
                self.serializer_class = GeositeSerializerPolygons
            elif geom_type.lower() == "extent":
                self.bbox_filter_field = "geom_extent"
                self.serializer_class = GeositeSerializerExtent

            else:
                raise ParseError("Invalid geometry type specified.")
        else:
            self.bbox_filter_field = "geom_point"
            self.serializer_class = GeositeSerializerExtent

        return queryset
    
class SitePhotographViewset(viewsets.ReadOnlyModelViewSet):
    serializer_class = SitePhotographSerializer
    def get_queryset(self):
        geosite_id = self.request.query_params.get("geosite_id")
        if geosite_id is not None:
            queryset = SitePhotograph.objects.filter(geosite=geosite_id)
            queryset = queryset.filter(approved=True)
            return queryset
        else:
            self.bbox_filter_field = "camera_location"
            self.bbox_filter_include_overlapping = True
            self.filter_backends = (filters.InBBoxFilter,)
            return SitePhotograph.objects.all()