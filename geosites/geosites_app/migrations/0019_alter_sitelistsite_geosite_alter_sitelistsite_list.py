# Generated by Django 4.2 on 2023-11-23 23:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('geosites_app', '0018_historicalsitelist_published_sitelist_published'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitelistsite',
            name='geosite',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='geosites_app.geosite'),
        ),
        migrations.AlterField(
            model_name='sitelistsite',
            name='list',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='geosites_app.sitelist'),
        ),
    ]
