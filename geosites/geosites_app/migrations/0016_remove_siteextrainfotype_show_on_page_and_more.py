# Generated by Django 4.2 on 2023-10-21 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geosites_app', '0015_siteextrainfotype_show_on_page_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='siteextrainfotype',
            name='show_on_page',
        ),
        migrations.AddField(
            model_name='gcrsite',
            name='full_gcr_link',
            field=models.URLField(db_column='full_gcr_link', editable=False, null=True, verbose_name='Link to full GCR site description'),
        ),
        migrations.AddField(
            model_name='historicalgcrsite',
            name='full_gcr_link',
            field=models.URLField(db_column='full_gcr_link', editable=False, null=True, verbose_name='Link to full GCR site description'),
        ),
    ]
