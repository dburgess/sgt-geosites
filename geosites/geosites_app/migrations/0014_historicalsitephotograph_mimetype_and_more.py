# Generated by Django 4.2 on 2023-09-18 15:56

from django.db import migrations, models
import easy_thumbnails.fields
import geosites_app.models


class Migration(migrations.Migration):

    dependencies = [
        ('geosites_app', '0013_alter_historicalsitephotograph_camera_location_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalsitephotograph',
            name='mimetype',
            field=models.TextField(blank=True, db_column='mimetype', editable=False, null=True),
        ),
        migrations.AddField(
            model_name='sitephotograph',
            name='mimetype',
            field=models.TextField(blank=True, db_column='mimetype', editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='historicalsitephotograph',
            name='file',
            field=models.TextField(max_length=100),
        ),
        migrations.AlterField(
            model_name='sitephotograph',
            name='file',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to=geosites_app.models.get_new_upload_path),
        ),
    ]
