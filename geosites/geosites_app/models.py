from django.contrib.gis.db import models
from django.conf import settings
from django.urls import reverse
from simple_history.models import HistoricalRecords
from easy_thumbnails.fields import ThumbnailerImageField

from .validators import CompassBearingValidators, NoFutureDateValidator

import datetime
import os
import uuid

class NatGridPointField(models.PointField):
    pass

class AdminArea(models.Model):
    def __str__(self):
        return f"{self.translation}"

    class Meta:
        db_table = "dic_gcr_admin_area"
        ordering = ["translation"]

    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=100, db_column="translation")

class GCRVolume(models.Model):
    def __str__(self):
        return f"({self.vol_number}) {self.title}"

    class Meta:
        db_table = "gcr_gcr_volume"
        ordering = ["vol_number"]

    vol_number = models.SmallIntegerField(db_column="vol_number")
    title = models.CharField(max_length=100, db_column="title")
    year_published = models.SmallIntegerField(db_column="year_published", null=True, blank=True)
    url = models.URLField(db_column="url", null=True, blank=True)

class BlockName(models.Model):
    def __str__(self):
        return f"{self.translation}"
    
    class Meta:
        db_table = "dic_gcr_block_name"
        ordering = ["translation"]

    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=100, db_column="translation")
    color = models.CharField(blank=False, max_length=10, db_column="color")
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, db_column="parent_block")
    gcr_volume = models.ForeignKey(GCRVolume, db_index=False, on_delete=models.PROTECT, to_field="id", db_column="gcr_volume_id", null=True, blank=True)

class GCRStatusCode(models.Model):
    def __str__(self):
        return f"{self.translation}"
    
    class Meta:
        db_table = "dic_gcr_site_status"
        ordering = ["translation"]
    
    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=20, db_column="translation")

class Agency(models.Model):
    def __str__(self):
        return f"{self.translation}"
    
    class Meta:
        db_table = "dic_gcr_agency"
        ordering = ["translation"]
    
    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=100, db_column="translation")

class ChronoUnit(models.Model):
    def __str__(self):
        return f"{self.translation}"
    
    class Meta:
        db_table = "dic_gcr_chrono_unit"
        ordering = ["translation"]
    
    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=20, db_column="translation")
    rank = models.PositiveSmallIntegerField(null=True, db_column="rank")
    parent_unit = models.ForeignKey("self", on_delete=models.CASCADE, null=True, db_column="parent_unit")

class SiteExtraInfoType(models.Model):
    def __str__(self):
        return f"{self.translation}"
    
    class Meta:
        db_table = "dic_gcr_site_extra_info_type"
        ordering = ["translation"]
    
    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=50, db_column="translation")
    link = models.BooleanField(blank=False, null=False, default=False)

class SitePhotographType(models.Model):
    def __str__(self):
        return f"{self.translation}"
    
    class Meta:
        db_table = "dic_site_photograph_type"
        ordering = ["translation"]
    
    code = models.CharField(primary_key=True, max_length=5, db_column="code")
    translation = models.CharField(blank=False, max_length=20, db_column="translation")

class Geosite(models.Model):
    def __str__(self):
        return f"{self.name} ({self.id})"

    def get_absolute_url(self):
        return reverse("geosite_detail", kwargs={"geosite_id": self.pk})
    

    class Meta:
        db_table = "gcr_geosite"
        ordering = ["name"]
        verbose_name = "geosite"

    name = models.CharField(blank=False, max_length=100, db_column="name", null=True)
    parent_id = models.ForeignKey("self", on_delete=models.CASCADE, db_column="parent_geosite_id", null=True, verbose_name="Parent site", editable=False)
    grid_ref = models.CharField("Grid reference", blank=False, max_length=10, db_column="grid_ref", null=True, editable=False)
    lon = models.FloatField("Latitude", db_column="lon", null=True, blank=True, editable=False)
    lat = models.FloatField("Longitude", db_column="lat", null=True, blank=True, editable=False)
    easting = models.IntegerField(db_column="easting", editable=False)
    northing = models.IntegerField(db_column="northing", editable=False)
    headline = models.TextField("Headline description", db_column="headline", max_length=500, null=True, blank=True, help_text="Aim for around 50 words")
    summary = models.TextField("Summary", db_column="summary", max_length=2000)
    visible = models.BooleanField("Visible", db_column="visible", null=False, blank=False, default=True, editable=False)
    geom_point = models.GeometryField("Point location", srid=27700, db_column="geom_point", default=None, null=True, editable=False)
    geom = models.GeometryField("Site boundary", srid=27700, db_column="geom_polygon", default=None, null=True, editable=False)
    geom_extent = models.GeometryField("Extent", srid=27700, db_column="geom_extent", default=None, null=True, editable=False)
    notes = models.TextField("Site notes", blank=True, null=True, db_column="notes")
    history = HistoricalRecords()

class GCRSite(models.Model):
    def __str__(self):
        geosite = Geosite.objects.filter(id=self.geosite_id).first()
        return f"{geosite.name} ({geosite.id})"

    class Meta:
        db_table = "gcr_gcrsite"
        ordering = ["geosite__name"]
        verbose_name = "GCR site"
    
    geosite = models.OneToOneField(Geosite, on_delete=models.PROTECT, parent_link=True, primary_key=True, db_column="geosite_id", editable=False)
    number = models.PositiveSmallIntegerField("GCR site number", db_column="number", editable=False)
    admin_area = models.ForeignKey(AdminArea, db_index=False, on_delete=models.PROTECT, to_field="code", db_column="admin_area_code", verbose_name="Administrative area", editable=False)
    block_name = models.ForeignKey(BlockName, db_index=False, on_delete=models.PROTECT, to_field="code", db_column="block_name_code", verbose_name="GCR block", editable=False)
    status = models.ForeignKey(GCRStatusCode, db_index=False, on_delete=models.PROTECT, to_field="code", db_column="status_code")
    date = models.DateField("GCR date", db_column="date", null=True, editable=False)
    review_date = models.DateField("Last review date", db_column="review_date", null=True, blank=True, editable=False)
    agency  = models.ForeignKey(Agency, db_index=False, on_delete=models.PROTECT, db_column="agency_code", verbose_name="Responsible agency", editable=False)
    statement = models.TextField("GCR short statement", db_column="statement", null=True, editable=False)
    chrono_unit = models.ForeignKey(ChronoUnit, db_index=False, on_delete=models.PROTECT, to_field="code", db_column="chrono_unit_code", verbose_name="Geochronological unit", editable=False)
    sssi_site_id = models.ForeignKey(Geosite, on_delete=models.PROTECT, related_name="sssi_sites", db_column="sssi_site_id", null=True, blank=True, verbose_name="SSSI site"),
    history = HistoricalRecords()
    has_national_archive_pdf = models.BooleanField("Has National Archive PDF", db_column="national_archive_pdf_yn", null=False, blank=False, default=False, editable=False)
    full_gcr_link = models.URLField("Link to full GCR site description", db_column="full_gcr_link", editable=False, null=True)

def get_new_upload_path(model, filename):
    _, ext = os.path.splitext(filename)
    return os.path.join("site_photographs", str(uuid.uuid4()) + ext)

class SitePhotograph(models.Model):
    def __str__(self):
        return f"{self.type.translation} | {self.date_entered.strftime('%Y-%m-%d %H:%M:%S')} | {self.file} | {self.user_entered}"
    
    class Meta:
        db_table = "gcr_site_photo"
        ordering = ["order"]
        verbose_name = "photograph"

    file = ThumbnailerImageField(upload_to=get_new_upload_path)
    type = models.ForeignKey(SitePhotographType, null=False, blank=False, on_delete=models.CASCADE, to_field="code", db_column="type", related_name="photographs")
    caption = models.TextField("Caption", null=True, blank=True)
    geosite = models.ForeignKey(Geosite, on_delete=models.CASCADE, to_field="id", db_column="geosite_id", related_name="photographs")
    approved = models.BooleanField(default=True)
    date = models.DateField(help_text="The date the photo was taken", null=True, blank=True, validators=[NoFutureDateValidator])
    user_approved = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column="user_approved_id", related_name="report_photos_approved", null=True, blank=True)
    camera_location = models.PointField("Camera location", srid=27700, db_column="camera_location", default=None, null=True, blank=True, help_text="The camera location field will be used to place your image on the map.")
    subject_location = models.PointField("Subject location", srid=27700, db_column="subject_location", default=None, null=True, blank=True, editable=False)
    look_direction = models.PositiveSmallIntegerField("View direction (bearing)", db_column = "look_direction", null=True, blank=True, validators=CompassBearingValidators)
    date_entered = models.DateTimeField(default=datetime.datetime.now, editable=False)
    user_entered = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column="user_id", editable=False, related_name="photographs")
    history = HistoricalRecords()
    order = models.PositiveSmallIntegerField("Order", null=True, blank=True)
    mimetype = models.TextField(editable=False, db_column="mimetype", null=True, blank=True)

class SiteExtraInfo(models.Model):
    def __str__(self):
        return f"{self.type}: {self.content}"
    
    class Meta:
        db_table = "gcr_site_extra_info"
        ordering = ["order"]
        verbose_name_plural = "additional site information"
        verbose_name = "additional site information"
    
    type = models.ForeignKey(SiteExtraInfoType, null=False, blank=False, on_delete=models.CASCADE, to_field="code", db_column="info_type", related_name="extra_info")
    link = models.URLField("Link", null=True, blank=True, db_column="link", help_text="If you're linking to another website (e.g. Geograph or Earthwise), add the URL here")
    content = models.TextField("Information", null=False, blank=False, max_length=500, help_text="Text to show on the geosite page")
    geosite = models.ForeignKey(Geosite, on_delete=models.CASCADE, to_field="id", db_column="geosite_id", related_name="extra_info", editable=False)
    date_entered = models.DateTimeField(auto_now_add=True, editable=False)
    user_entered = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, db_column="user_id", editable=False, related_name="extra_info")
    history = HistoricalRecords()
    order = models.PositiveSmallIntegerField("Order", null=True, blank=True)

class SiteList(models.Model):
    def __str__(self):
        return f"{self.name}"
    
    class Meta:
        db_table = "gcr_site_list"

    name = models.TextField("Name", null=False, blank=False, max_length=500, help_text="A name for your list")
    description = models.TextField("List description", null=True)
    sites = models.ManyToManyField(Geosite, through="SiteListSite")
    url_slug = models.TextField(null=False, blank=False, unique=True, max_length=30, help_text="The URL you would like to share for your list")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, editable=False, related_name="site_lists")
    date_entered = models.DateTimeField(auto_now_add=True, editable=False)
    published = models.BooleanField(null=False, default=False)
    history = HistoricalRecords()

class SiteListSite(models.Model):
    class Meta:
        db_table = "gcr_site_list_site"

    geosite = models.ForeignKey(Geosite, on_delete=models.CASCADE)
    list = models.ForeignKey(SiteList, on_delete=models.CASCADE)
    order_in_list = models.PositiveSmallIntegerField("Order", null=True, blank=True)