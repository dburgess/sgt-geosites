const transformCoords = function(arr) {
    return proj4('EPSG:27700', 'EPSG:4326', arr).reverse();
};

function assert(condition, message) {
    if (!condition) throw new Error(message || "Assertion failed");
}

const POINTS_TO_POLYGONS_ZOOMLEVEL = 6;
const CRS_STRING = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +towgs84=446.448,-125.157,542.06,0.15,0.247,0.842,-20.489 +units=m +no_defs";

proj4.defs("EPSG:27700", CRS_STRING);
const crs = new L.Proj.CRS('EPSG:27700', CRS_STRING, {
    resolutions: [ 896.0, 448.0, 224.0, 112.0, 56.0, 28.0, 14.0, 7.0, 3.5, 1.75 ],
    origin: [ -238375.0, 1376256.0 ]
});

// Initialize the map
const mapOptions = {
    minZoom: 1,
    maxZoom: 8,
    center: JSON.parse(localStorage.getItem("map_center")) || [ 56.743, -4.374 ],
    zoom: Number(localStorage.getItem("map_zoom")) || 1,
    maxBounds: [
        transformCoords([ -238375.0, 0.0 ]),
        transformCoords([ 900000.0, 1376256.0 ])
    ],
    attributionControl: false,
    crs: crs,
};

const map = L.map('map', mapOptions);

const year = new Date().getFullYear()
os_statement = `Contains OS data © Crown copyright and database rights ${year}`
bgs_statement = `Contains British Geological Survey materials © UKRI ${year}`

const os_basemap = L.tileLayer(`https://api.os.uk/maps/raster/v1/zxy/Leisure_27700/{z}/{x}/{y}.png?key=${OS_API_KEY}`, {
    maxZoom: 20,
    attribution: os_statement,
}).addTo(map);
const bgs_bedrock_50k = L.tileLayer.wms('https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?', {
    layers: "BGS.50k.Bedrock",
    opacity: 0.3,
    format: "image/png",
    minZoom: 6,
    transparent: true,
    attribution: bgs_statement
});
const bgs_superficial_50k = L.tileLayer.wms('https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?', {
    layers: "BGS.50k.Superficial.deposits",
    opacity: 0.3,
    format: "image/png",
    minZoom: 6,
    transparent: true,
    attribution: bgs_statement
});
const bgs_bedrock_625k = L.tileLayer.wms('https://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/ows?', {
    layers: "GBR_BGS_625k_BLT",
    opacity: 0.3,
    format: "image/png",
    maxZoom: 5,
    transparent: true,
    attribution: bgs_statement
});
const bgs_superficial_625k = L.tileLayer.wms('https://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/ows?', {
    layers: "GBR_BGS_625k_SLT",
    opacity: 0.3,
    format: "image/png",
    maxZoom: 5,
    transparent: true,
    attribution: bgs_statement
});

let bgs_bedrock = L.layerGroup([bgs_bedrock_50k, bgs_bedrock_625k])
let bgs_superficial = L.layerGroup([bgs_superficial_50k, bgs_superficial_625k])
let overlay_maps = {
    "BGS Superficial Geology": bgs_superficial,
    "BGS Bedrock Geology": bgs_bedrock,
}

let base_maps = {
    "Ordnance Survey": os_basemap,
};

let layercontrol = L.control.layers(null, overlay_maps, {
    collapsed: false
}).addTo(map);

L.control.attribution({
    prefix: false,
    position: 'bottomright'
}).addTo(map);

// The current map overlays with the geosite geometries and site photos
let overlay = {};
let photo_overlay = {};
// Object to hold state of layers on map - load from local storage
let overlay_state = JSON.parse(localStorage.getItem("geosites_overlay_state")) || {
    "overlays": {
        "gmorp": false,
        "igpet": false,
        "minrl": false,
        "paleo": false,
        "quatg": false,
        "stmmg": false,
        "strat": false,
        "photos": false,
    },
};

// Initialise the checkboxes
for (let [overlay_id, enabled] of Object.entries(overlay_state["overlays"])) {
    document.getElementById("checkbox-" + overlay_id).checked = enabled;
}
refresh_overlays();

function truncate(str, n){
    return (str.length > n) ? str.slice(0, n-1) + '&hellip;' : str;
};

function toggle_layer(layer_id) {
    overlay_state["overlays"][layer_id] = !overlay_state["overlays"][layer_id];
    localStorage.setItem("geosites_overlay_state", JSON.stringify(overlay_state));
    refresh_overlays();
}

function show_geosite_details(current_geosite) {
    document.getElementById("geosites-intro-box").style.display = "none";
    document.getElementById("geosite-name").innerHTML = current_geosite.properties.name;
    document.getElementById("geosite-headline").innerHTML = current_geosite.properties.headline || "";
    document.getElementById("gcrsite-block-name").innerHTML = current_geosite.properties.gcrsite_block_name;
    document.getElementById("gcrsite-parent-block-name").innerHTML = current_geosite.properties.gcrsite_parent_block_name;
    document.getElementById("geosite-attribs-box").style.display = 'block';

    detail_url = DETAIL_BASE_URL + current_geosite.id
    document.getElementById("gcrsite-detail-link").href = detail_url;
}

/* Return bounding box string of current map view */
function map_get_bbox() {
    bounds = map.getBounds();
    pt_sw = proj4(CRS_STRING, [bounds.getSouthWest().lng, bounds.getSouthWest().lat]);
    pt_ne = proj4(CRS_STRING, [bounds.getNorthEast().lng, bounds.getNorthEast().lat]);
    return `${pt_sw[0]},${pt_sw[1]},${pt_ne[0]},${pt_ne[1]}`;
}

/* Return a JSON list of geosites satisfying the critera passed in the params object:
 *      params.geom:    points, polygons or null, depending on the geometry to return
 *      params.blocks:  list of GCR parent blocks to return (five letter codes)
 *      params.bbox:    bounding box in which to search
 */
async function api_get_list(bbox, blocks=[], geom=null) {
    assert(["point", "polygon", "extent", null].includes(geom), "Invalid geometry type");
    let url = "api/geosites/?";
    if (bbox) url += `&in_bbox=${bbox}`;
    if (blocks.length != 0) url += `&blocks=${blocks.toString()}`;
    if (geom) url += `&geom=${geom}`;
    const response = await fetch(url);
    const json = await response.json();
    return json;
}

/* Return a JSON object containing the detail for a single geosite:
 *      geosite_id:     primary key of that geosite
 *      geom:           point, poly or null, depending on the geometry to return
 */
async function api_get_detail(geosite_id, geom=null) {
    assert(["point", "polygon", "extent", null].includes(geom), "Invalid geometry type");
    let url = `/api/geosites/${geosite_id}`;
    if (geom) {
        url = url + `/?${geom}`;
    }
    const response = await fetch(url + "/");
    const json = await response.json();
    return json;
}

async function load_photograph_geojson_bbox(bbox) {
    const url = `/api/geositephotos/?&in_bbox=${bbox}`;
    const response = await fetch(url);
    const geojson = await response.json();
    return geojson;
}

let photo_icon = L.icon({
    "iconUrl": PHOTO_ICON_URL,
    "iconSize": [25, 25],
    "popupAnchor": [0, 0],
})

async function add_photos() {
    let geojson = await load_photograph_geojson_bbox(map_get_bbox());
    photo_overlay = L.Proj.geoJson(geojson, {
        pointToLayer: function (feature, latlng) {
            let popup_content = `<a href=${feature.properties.file}><img src=${feature.properties.file}></a><a href="${DETAIL_BASE_URL + feature.properties.geosite}"><strong>${feature.properties.geosite_name}</strong></a><br>${feature.properties.caption}`;
            return L.marker(latlng, {
                "icon": photo_icon,
            }).bindTooltip(truncate(feature.properties.caption, 40)).bindPopup(popup_content);
        },
    }).addTo(map);
}

async function refresh_overlays() {
    if (map.hasLayer(overlay)) {
        map.removeLayer(overlay);
        overlay = {}
    }
    if (map.hasLayer(photo_overlay)) {
        map.removeLayer(photo_overlay);
        photo_overlay = {}
    }

    let blocks_enabled = [];
    for (let [overlay_id, enabled] of Object.entries(overlay_state["overlays"])) {
        if (enabled) blocks_enabled.push(overlay_id);
    }

    // Don't display anything if no blocks are selected
    if (!blocks_enabled.length) return;

    if (map.getZoom() >= POINTS_TO_POLYGONS_ZOOMLEVEL) geom_type = "polygon";
    else geom_type = "point";
    
    const geojson = await api_get_list(map_get_bbox(), blocks_enabled, geom_type);

    if (geom_type == "point") {
        if (map.hasLayer(overlay)) return;
        overlay = L.Proj.geoJson(geojson, {
            pointToLayer: function (feature, latlng) {
                let html=`<div style="height: 1em; width: 1em;
                    background:${feature.properties.gcrsite_parent_block_color}6e;
                    border:3px solid ${feature.properties.gcrsite_parent_block_color};" />`
                return L.marker(latlng, {
                    icon: L.divIcon({className: 'marker-icon', html: html}),
                }).bindTooltip(feature.properties.name);
            },
            onEachFeature: function (feature, layer) {
                layer.on({
                    click: select_geosite,
                })
            }
        }).addTo(map);
    } else if (geom_type == "polygon") {
        if (map.hasLayer(overlay)) return; // Required because async behaviour means that multiple requests may be made at once, leading to multiple layers being added
        overlay = L.Proj.geoJson(geojson, {
            style: (feature) => {
                return {
                    fillColor: feature.properties.gcrsite_parent_block_color,
                    weight: 2,
                    opacity: 0.5,
                    color: "black",
                    fillOpacity: 0.5,
                }
            },
            onEachFeature: function (feature, layer) {
                layer.on("mouseover", function () {
                    this.setStyle({
                        weight: 5
                    });
                });
                layer.on("mouseout", function () {
                    overlay.resetStyle(this);
                });
                layer.on("click", select_geosite);
                layer.bindTooltip(feature.properties.name);
            }
        }).addTo(map);
    }
    if (document.getElementById("checkbox-photos").checked) add_photos();
}

async function select_geosite(e) {
    const selected_geosite = e.target.feature;
    details = await api_get_detail(selected_geosite.id);
    let details_geojson = L.Proj.geoJson(details);
    map.fitBounds(details_geojson.getBounds());
    show_geosite_details(details);
}

map.on("moveend", function () {
    refresh_overlays();
    localStorage.setItem("map_center", JSON.stringify(map.getCenter()));
});

map.on("zoomend", function () {
    refresh_overlays();
    localStorage.setItem("map_zoom", map.getZoom().toString());
})