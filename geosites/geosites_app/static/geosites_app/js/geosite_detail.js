let current_photo = null;
let geosite_geom = L.Proj.geoJson();
let photo_geom = L.Proj.geoJson();

function truncate(str, n){
    return (str.length > n) ? str.slice(0, n-1) + '&hellip;' : str;
};

async function load_photograph_geojson(geosite_id) {
    const url = `/api/geositephotos/?geosite_id=${geosite_id}`;
    const response = await fetch(url);
    const geojson = await response.json();
    return geojson;
}

let photo_icon = L.icon({
    "iconUrl": PHOTO_ICON_URL,
    "iconSize": [25, 25],
    "popupAnchor": [0, 0],
})

async function add_photos(geosite_id) {
    geojson = await load_photograph_geojson(geosite_id);
    photo_geom = L.Proj.geoJson(geojson, {
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                "icon": photo_icon,
            }).bindTooltip(truncate(feature.properties.caption, 40))
        },
        onEachFeature: function (feature, layer) {
            layer.on({
                click: (e) => {
                map.setView(e.latlng, 7);
                let selected_photo = document.getElementById(`photo-${feature.id}`)
                selected_photo.scrollIntoView();
                if (current_photo) current_photo.style.boxShadow = "";
                selected_photo.style.boxShadow = "0 0 15px 10px rgba(252, 213, 40, 0.7)";
                current_photo = selected_photo;
                }
            })
        }
    }).addTo(map);

    let layers = L.featureGroup([geosite_geom, photo_geom]);
    map.fitBounds(layers.getBounds(), {animate: false});
    map.setMaxBounds(map.getBounds());
    map.setMinZoom(map.getZoom());
}

async function load_geojson_by_id(geosite_id) {
    const url = `/api/geosites/${geosite_id}/?geom=polygon`;
    const response = await fetch(url);
    const geojson = await response.json();
    return geojson;
}

async function add_geosite(geosite_id) {
    geojson = await load_geojson_by_id(geosite_id);
    geosite_geom = L.Proj.geoJson(geojson, {
        style: geosite_style
    });
    geosite_geom.addTo(map);
    add_photos(CURRENT_GEOSITE_ID);
}

function geosite_style(feature) {
    return {
        fillColor: "blue",
        weight: 3,
        opacity: 0.5,
        color: "blue",
        fillOpacity: 0.1,
        dashArray: "10"
    };
}

const transformCoords = function(arr) {
    return proj4('EPSG:27700', 'EPSG:4326', arr).reverse();
};

const crs_string = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +towgs84=446.448,-125.157,542.06,0.15,0.247,0.842,-20.489 +units=m +no_defs";
proj4.defs("EPSG:27700", crs_string);

const crs = new L.Proj.CRS('EPSG:27700', crs_string, {
    resolutions: [ 896.0, 448.0, 224.0, 112.0, 56.0, 28.0, 14.0, 7.0, 3.5, 1.75 ],
    origin: [ -238375.0, 1376256.0 ]
});

// Initialize the map
const mapOptions = {
    minZoom: 1,
    maxZoom: 8,
    center: [ 56.743, -4.374 ],
    zoom: 1,
    maxBounds: [
        transformCoords([ -238375.0, 0.0 ]),
        transformCoords([ 900000.0, 1376256.0 ])
    ],
    attributionControl: false,
    crs: crs,
};

const map = L.map('map', mapOptions);

const year = new Date().getFullYear()
os_statement = `Contains OS data © Crown copyright and database rights ${year}`
bgs_statement = `Contains British Geological Survey materials © UKRI ${year}`

const os_basemap = L.tileLayer(`https://api.os.uk/maps/raster/v1/zxy/Leisure_27700/{z}/{x}/{y}.png?key=${OS_API_KEY}`, {
    maxZoom: 20,
    attribution: os_statement,
}).addTo(map);
const bgs_bedrock_50k = L.tileLayer.wms('https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?', {
    layers: "BGS.50k.Bedrock",
    opacity: 0.3,
    format: "image/png",
    minZoom: 6,
    transparent: true,
    attribution: bgs_statement
});
const bgs_superficial_50k = L.tileLayer.wms('https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?', {
    layers: "BGS.50k.Superficial.deposits",
    opacity: 0.3,
    format: "image/png",
    minZoom: 6,
    transparent: true,
    attribution: bgs_statement
});
const bgs_bedrock_625k = L.tileLayer.wms('https://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/ows?', {
    layers: "GBR_BGS_625k_BLT",
    opacity: 0.3,
    format: "image/png",
    maxZoom: 5,
    transparent: true,
    attribution: bgs_statement
});
const bgs_superficial_625k = L.tileLayer.wms('https://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/ows?', {
    layers: "GBR_BGS_625k_SLT",
    opacity: 0.3,
    format: "image/png",
    maxZoom: 5,
    transparent: true,
    attribution: bgs_statement
});
var baselayers = {
    "OS Leisure": os_basemap
}
var bgs_bedrock = L.layerGroup([bgs_bedrock_50k, bgs_bedrock_625k])
var bgs_superficial = L.layerGroup([bgs_superficial_50k, bgs_superficial_625k])
var overlay_maps = {
    "BGS Superficial Geology": bgs_superficial,
    "BGS Bedrock Geology": bgs_bedrock
}

var layercontrol = L.control.layers(base_maps, overlay_maps, {
    collapsed: false
}).addTo(map);

L.control.attribution({
    prefix: false,
    position: 'bottomright'
}).addTo(map);

var base_maps = {
    "Ordnance Survey": os_basemap,
};

add_geosite(CURRENT_GEOSITE_ID);