const transformCoords = function(arr) {
    return proj4('EPSG:27700', 'EPSG:4326', arr).reverse();
};

function assert(condition, message) {
    if (!condition) throw new Error(message || "Assertion failed");
}

const POINTS_TO_POLYGONS_ZOOMLEVEL = 6;
const CRS_STRING = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +towgs84=446.448,-125.157,542.06,0.15,0.247,0.842,-20.489 +units=m +no_defs";

proj4.defs("EPSG:27700", CRS_STRING);
const crs = new L.Proj.CRS('EPSG:27700', CRS_STRING, {
    resolutions: [ 896.0, 448.0, 224.0, 112.0, 56.0, 28.0, 14.0, 7.0, 3.5, 1.75 ],
    origin: [ -238375.0, 1376256.0 ]
});

// Initialize the map
const mapOptions = {
    minZoom: 0,
    maxZoom: 8,
    center:  [ 56.743, -4.374 ],
    zoom: 1,
    maxBounds: [
        transformCoords([ -238375.0, 0.0 ]),
        transformCoords([ 900000.0, 1376256.0 ])
    ],
    attributionControl: false,
    crs: crs,
};

const map = L.map('map', mapOptions);

const year = new Date().getFullYear()
os_statement = `Contains OS data © Crown copyright and database rights ${year}`
bgs_statement = `Contains British Geological Survey materials © UKRI ${year}`

const os_basemap = L.tileLayer(`https://api.os.uk/maps/raster/v1/zxy/Leisure_27700/{z}/{x}/{y}.png?key=${OS_API_KEY}`, {
    maxZoom: 20,
    attribution: os_statement,
}).addTo(map);
const bgs_bedrock_50k = L.tileLayer.wms('https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?', {
    layers: "BGS.50k.Bedrock",
    opacity: 0.3,
    format: "image/png",
    minZoom: 6,
    transparent: true,
    attribution: bgs_statement
});
const bgs_superficial_50k = L.tileLayer.wms('https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?', {
    layers: "BGS.50k.Superficial.deposits",
    opacity: 0.3,
    format: "image/png",
    minZoom: 6,
    transparent: true,
    attribution: bgs_statement
});
const bgs_bedrock_625k = L.tileLayer.wms('https://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/ows?', {
    layers: "GBR_BGS_625k_BLT",
    opacity: 0.3,
    format: "image/png",
    maxZoom: 5,
    transparent: true,
    attribution: bgs_statement
});
const bgs_superficial_625k = L.tileLayer.wms('https://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/ows?', {
    layers: "GBR_BGS_625k_SLT",
    opacity: 0.3,
    format: "image/png",
    maxZoom: 5,
    transparent: true,
    attribution: bgs_statement
});

let bgs_bedrock = L.layerGroup([bgs_bedrock_50k, bgs_bedrock_625k])
let bgs_superficial = L.layerGroup([bgs_superficial_50k, bgs_superficial_625k])
let overlay_maps = {
    "BGS Superficial Geology": bgs_superficial,
    "BGS Bedrock Geology": bgs_bedrock,
}

let base_maps = {
    "Ordnance Survey": os_basemap,
};

let layercontrol = L.control.layers(null, overlay_maps, {
    collapsed: false
}).addTo(map);

L.control.attribution({
    prefix: false,
    position: 'bottomright'
}).addTo(map);

let allSites = L.Proj.geoJson(JSON.parse(SITE_LOCATIONS), {
    pointToLayer: function (feature, latlng) {
        let html=`<div style="height: 2em; width: 2em;
            background: #1d51fe6e;
            border:6px solid #1d51fe;"/>`
        return L.marker(latlng, {
            icon: L.divIcon({className: 'marker-class', html: html}),
        }).bindTooltip(feature.properties.name);
    }
}).addTo(map);
map.fitBounds(allSites.getBounds().pad(0.3))


/* Return bounding box string of current map view */
function map_get_bbox() {
    bounds = map.getBounds();
    pt_sw = proj4(CRS_STRING, [bounds.getSouthWest().lng, bounds.getSouthWest().lat]);
    pt_ne = proj4(CRS_STRING, [bounds.getNorthEast().lng, bounds.getNorthEast().lat]);
    return `${pt_sw[0]},${pt_sw[1]},${pt_ne[0]},${pt_ne[1]}`;
}



