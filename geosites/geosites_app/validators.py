from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator
from .utils import GRID_REF_REGEX

import datetime
import re

GridRefValidator = RegexValidator(GRID_REF_REGEX, "Invalid grid reference", flags=re.I)
NoFutureDateValidator = MaxValueValidator(datetime.date.today, "Enter a date that is either today, or in the past")
CompassBearingValidators = [ MaxValueValidator(359, "Invalid bearing"), MinValueValidator(0, "Invalid bearing") ]