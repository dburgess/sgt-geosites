from django.conf import settings

def analytics_processor(request):
    return {
        "USE_ANALYTICS": settings.USE_ANALYTICS
    }