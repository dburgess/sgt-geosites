from django.apps import AppConfig

class GeositesAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'geosites_app'
