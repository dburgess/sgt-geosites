import re
import magic
import math

GRID_REF_REGEX = r"^([HNOST][ABCDEFGHJKLMNOPQRSTUVWXYZ]) ?(?:(?:(\d{5}) ?(\d{5}))|(?:(\d{4}) ?(\d{4}))|(?:(\d{3}) ?(\d{3})))$"

def convert_to_gridref(x, y, prec):
    FIRST_LETTERS = [
        ["S", "T"],
        ["N", "O"],
        ["H", "J"]
    ]
    SECOND_LETTERS = [
        ["V", "W", "X", "Y", "Z"],
        ["Q", "R", "S", "T", "U"],
        ["L", "M", "N", "O", "P"],
        ["F", "G", "H", "J", "K"],
        ["A", "B", "C", "D", "E"]
    ]

    x_100s =  math.floor(x / 100000)
    y_100s = math.floor(y / 100000)
    x_remainder = math.floor((x % 100000) / 10**(5-(prec/2)))
    y_remainder = math.floor((y % 100000) / 10**(5-(prec/2)))

    first_letter = FIRST_LETTERS[math.floor(y_100s/5)][math.floor(x_100s/5)]
    second_letter = SECOND_LETTERS[y_100s % 5][x_100s % 5]

    return f"{first_letter}{second_letter} " + str(x_remainder).zfill(int(prec/2)) + " " + str(y_remainder).zfill(int(prec/2))


def convert_to_coordinates(grid_ref_str, offset_to_square_center=False):
        ret = re.search(GRID_REF_REGEX, grid_ref_str.upper())
        if not ret:
            return None

        offset = 0

        letters = ret.group(1)
        if ret.group(2):
            x = int(ret.group(2))
            offset = 0.5
        elif ret.group(4):
            x = int(ret.group(4)) * 10
            offset = 5
        elif ret.group(6):
            x = int(ret.group(6)) * 100
            offset = 50
        if ret.group(3):
            y = int(ret.group(3))
        elif ret.group(5):
            y = int(ret.group(5)) * 10
        elif ret.group(7):
            y = int(ret.group(7)) * 100

        if letters[0] in ["O", "T"]:
            x += 500_000
        if letters[0] in ["N", "O"]:
            y += 500_000
        if letters[0] in ["H"]:
            y += 1_000_000
        if letters[1] in ["E", "K", "P", "U", "Z"]:
            x += 400_000
        if letters[1] in ["D", "J", "O", "T", "Y"]:
            x += 300_000
        if letters[1] in ["C", "H", "N", "S", "X"]:
            x += 200_000
        if letters[1] in ["B", "G", "M", "R", "W"]:
            x += 100_000
        if letters[1] in ["A", "B", "C", "D", "E"]:
            y += 400_000
        if letters[1] in ["F", "G", "H", "J", "K"]:
            y += 300_000
        if letters[1] in ["L", "M", "N", "O", "P"]:
            y += 200_000
        if letters[1] in ["Q", "R", "S", "T", "U"]:
            y += 100_000

        if offset_to_square_center:
            x += offset
            y += offset

        return (x, y)

# Credit to Pithikos
# https://stackoverflow.com/questions/4853581/django-get-uploaded-file-type-mimetype
def get_mime_type(file):
    """
    Get MIME by reading the header of the file
    """
    initial_pos = file.tell()
    file.seek(0)
    mime_type = magic.from_buffer(file.read(2048), mime=True)
    file.seek(initial_pos)
    return mime_type