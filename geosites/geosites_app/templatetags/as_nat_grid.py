from django import template
from geosites_app import utils

register = template.Library()

def as_nat_grid(value, prec=8):
    return utils.convert_to_gridref(value.x, value.y, prec)

register.filter("as_nat_grid", as_nat_grid)