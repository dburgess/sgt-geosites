from django.shortcuts import redirect, render, get_object_or_404
from django.conf import settings
from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView
from datetime import datetime
from django.template.response import TemplateResponse
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import PermissionDenied, BadRequest
from django.http import Http404

import html
import json
import logging

from .models import Geosite, SitePhotograph, SiteExtraInfo, SiteExtraInfoType, SiteList
from .forms import GeositeForm, ExtraInfoFormset, PhotographFormset, ExtraInfoFormsetHelper, PhotographFormsetHelper, UserForm, SiteListForm

logger = logging.getLogger(__name__)

def map(request):
    geosites = Geosite.objects.filter(gcrsite__agency="SNH").order_by("-name")[:10]
    context = {
        "geosites": geosites,
        "user": request.user,
        "user_is_staff": request.user.groups.filter(name="Staff").exists(),
        "user_is_superuser": request.user.is_superuser, 
        "OS_API_KEY": settings.OS_API_KEY,
    }
    logger.info(f"User ({request.user}) is viewing the map")
    return render(request, "geosites_app/geosites.html", context)

def view_account(request, user_id):
    viewed_user = get_object_or_404(User, id=user_id)
    if not request.user.is_superuser and viewed_user.id != request.user.id:
        raise PermissionDenied("You do not have permission to view this user.")
    
    user_form = None
    if request.method == "POST":
        logger.info(f"User ({request.user}) submitted form ({request.POST})")
        user_form = UserForm(request.POST, instance=viewed_user)
        if user_form.is_valid(): 
                user_form.save()
                messages.add_message(request, messages.SUCCESS, "Changes saved")
        else:
            for error in user_form.errors:
                messages.add_message(request, messages.ERROR, "There was a problem saving your changes. Please check your input.")
                logger.warning(f"User ({request.user}) encountered error while editing user ({viewed_user}): ({error})")

    context = {
        "user_form": user_form or UserForm(instance=viewed_user),
        "viewed_user": viewed_user,
        "user": request.user,
        "sitelists": SiteList.objects.filter(user=request.user)
    }
    logger.info(f"User ({request.user}) is viewing the account of ({viewed_user})")
    return render(request, "geosites_app/profile.html", context)

def about(request):
    context = {
        "current_year": datetime.now().year,
    }
    logger.info(f"User ({request.user}) is viewing the about page")
    return render(request, "geosites_app/about.html", context)

def list_recent_changes(request):
    photographs = SitePhotograph.objects.order_by("-date_entered")[:25]
    extra_info_entries = SiteExtraInfo.objects.order_by("-date_entered")[:25]
    sitelists = SiteList.objects.filter(published=True).order_by("-date_entered")[:25]
    logger.info(f"User ({request.user}) is viewing the recent changes page")

    def get_lowercase_type(type):
        if type in ["BGS Earthwise page", "BGS GeoScenic photo", "Geograph photo", "NatureScot SiteLink"]:
            return type
        else:
            return type.lower()

    entries = []
    for photograph in photographs:
        info = {
            "entry_type": "photograph",
            "date": photograph.date_entered,
            "user_name": photograph.user_entered.get_full_name() or photograph.user_entered.username,
            "geosite_id": photograph.geosite.id,
            "geosite_name": photograph.geosite.name,
            "type": "photograph"
        }
        entries.append(info)
    for extra_info in extra_info_entries:
        info = {
            "entry_type": "extra_info",
            "date": extra_info.date_entered,
            "user_name": extra_info.user_entered.get_full_name() or extra_info.user_entered.username,
            "geosite_id": extra_info.geosite.id,
            "geosite_name": extra_info.geosite.name,
            "type": get_lowercase_type(extra_info.type.translation)
        }
        entries.append(info)
    for sitelist in sitelists:
        info = {
            "entry_type": "sitelist",
            "date": sitelist.date_entered,
            "user_name": sitelist.user.get_full_name() or sitelist.user_entered.username,
            "name": sitelist.name,
            "slug":
             sitelist.url_slug,
        }
        entries.append(info)

    entries = sorted(entries, key=lambda dict: dict["date"], reverse=True)

    context = {
        "log_entries": entries
    }

    return TemplateResponse(request, "geosites_app/recent_changes_list.html", context)

class ListGeosites(ListView):
    model = Geosite
    paginate_by = 51

    def get_queryset(self):
        queryset = Geosite.objects.filter(gcrsite__agency="SNH").order_by('name')
        
        query = self.request.GET.get("q")
        if query:
            queryset = queryset.filter(name__icontains=query)
        
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListGeosites, self).get_context_data(**kwargs)
        sites_list = []
        for geosite in context["page_obj"]:
            sites_list.append({
                "geosite": geosite,
                "photo": geosite.photographs.filter(approved=True).order_by("?").first()
            })

        context["query"] = self.request.GET.get("q")
        context["sites_list"] = sites_list
        context["object_list"] = None
        return context

class GeositeDetail(DetailView):
    model = Geosite
    pk_url_kwarg = "geosite_id"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["geosite"] = get_object_or_404(Geosite, pk=self.kwargs["geosite_id"])
        context["approved_photos"] = context["geosite"].photographs.filter(approved=True)
        context["OS_API_KEY"] = settings.OS_API_KEY
        context["user_is_staff"] = self.request.user.groups.filter(name="Staff").exists()
        context["user_is_superuser"] = self.request.user.is_superuser
        context["user_sitelists"] = SiteList.objects.filter(user=self.request.user) if self.request.user.is_authenticated else None
        logger.info(f"User ({self.request.user}) is viewing geosite ({context['geosite'].name})")
        return context


def reset_object_order_if_invalid(qset):
    # If there are any objects with null order or there are duplicates in the order field, assign values by date_entered
    # This effectively resets the order to the order in which they were entered
    values_list = qset.values_list("order", flat=True)
    if None in values_list or len(set(values_list)) != len(values_list):
        all_in_qset = qset.order_by("date_entered")
        for counter, info in enumerate(all_in_qset):
            info.order=counter
            info.save()

def swap_order(obj1, obj2):
    order_swap = obj2.order
    obj2.order=obj1.order
    obj1.order=order_swap
    obj2.save()
    obj1.save()

@staff_member_required(login_url="/accounts/login")
def geosite_edit(request, geosite_id):

    geosite = get_object_or_404(Geosite.objects.all(), id = geosite_id)
    logger.info(f"User ({request.user}) is editing geosite ({geosite.name})")

    extra_info = SiteExtraInfo.objects.filter(geosite_id=geosite_id)
    photographs = SitePhotograph.objects.filter(geosite_id=geosite_id)
    if not request.user.is_superuser:
        extra_info = extra_info.filter(user_entered=request.user)
        photographs = photographs.filter(user_entered=request.user)

    context = {
        "geosite": geosite,
        "geosite_form": GeositeForm(instance=geosite),
        "extra_info": extra_info,
        "extra_info_formset": ExtraInfoFormset(queryset=extra_info, prefix="extra_info"),
        "photographs": photographs,
        "photographs_formset": PhotographFormset(queryset=photographs, prefix="photo"),
        "extra_info_formset_helper": ExtraInfoFormsetHelper(),
        "photographs_formset_helper": PhotographFormsetHelper(),
        "linkable_info_types": json.dumps(list(SiteExtraInfoType.objects.filter(link=True).values_list("code", flat=True)))
    }

    # Ensure the ordering of items is valid before attempting to edit them
    all_extra_info = SiteExtraInfo.objects.filter(geosite_id=geosite.id)
    all_photographs = SitePhotograph.objects.filter(geosite_id=geosite.id)
    reset_object_order_if_invalid(all_extra_info)
    reset_object_order_if_invalid(all_photographs)

    if request.method == "POST":
        logger.info(f"User ({request.user}) submitted form ({request.POST})")
        if "edit_geosite" in request.POST:
            geosite_form = GeositeForm(request.POST, instance=geosite)
            if geosite_form.is_valid(): 
                geosite_form.save()
                messages.add_message(request, messages.SUCCESS, "Changes saved")
            else:
                for error in geosite_form.errors:
                    messages.add_message(request, messages.ERROR, error)
                    logger.warning(f"User ({request.user}) encountered error while editing geosite ({geosite.name}): ({error})")
            context["geosite_form"] = geosite_form
        if "edit_extra_info" in request.POST:
            extra_info_formset = ExtraInfoFormset(request.POST, prefix="extra_info")
            if extra_info_formset.is_valid():
                new_entries = extra_info_formset.save(commit=False)
                for new_entry in new_entries:
                    new_entry.user_entered = request.user
                    new_entry.geosite = geosite
                    new_entry.order = all_extra_info.order_by("order").last().order + 1 if all_extra_info else 0
                for entry in extra_info_formset.deleted_objects:
                    entry.delete()
                extra_info_formset.save()
                extra_info_formset = ExtraInfoFormset(queryset=extra_info, prefix="extra_info")
                messages.success(request, "Your changes were saved.")
            else:
                if any(extra_info_formset.errors):
                    messages.error(request, "There was an error saving the additional information. Please check your input.")
                    for error in extra_info_formset.errors:
                        logger.warning(f"User ({request.user}) encountered error while editing extra_info for ({geosite.name}): ({error})")
            context["extra_info_formset"] = extra_info_formset
        if "edit_photograph" in request.POST:
            photographs_formset = PhotographFormset(request.POST, request.FILES, prefix="photo")
            if photographs_formset.is_valid():
                new_entries = photographs_formset.save(commit=False)
                for new_entry in new_entries:
                    new_entry.user_entered = request.user
                    new_entry.approved = True
                    new_entry.geosite = geosite
                    new_entry.order = all_photographs.order_by("order").last().order + 1 if all_photographs else 0
                for entry in photographs_formset.deleted_objects:
                    entry.delete()
                photographs_formset.save()
                photographs_formset = PhotographFormset(queryset=photographs, prefix="photo")
                messages.success(request, "Your changes were saved.")
            else:
                if any(photographs_formset.errors):
                    messages.error(request, "There was an error saving the photograph. Please check your input.")
                    for error in photographs_formset.errors:
                        logger.warning(f"User ({request.user}) encountered error while editing photographs for ({geosite.name}): ({error})")
            context["photographs_formset"] = photographs_formset

    return TemplateResponse(request, "geosites_app/geosite_edit.html", context)


@staff_member_required(login_url="/accounts/login")
def switch_order_extra_info(request, id, direction):
    extra_info = get_object_or_404(SiteExtraInfo.objects.all(), id = id)

    if not request.user.is_superuser and extra_info not in request.user.extra_info.all():
        logger.info(f"User ({request.user}) attempted to order extra_info for geosite ({extra_info.geosite.name} without permission)")
        raise PermissionDenied("You do not have permission to modify this information")
    if direction not in ["up", "down"]:
        logger.info(f"User ({request.user}) gave bad direction when ordering extra_info for geosite ({extra_info.geosite.name})")
        raise BadRequest("Invalid direction specified")

    selected = SiteExtraInfo.objects.get(id=id)
    order_selected = selected.order
    if direction == "up":
        swap = SiteExtraInfo.objects.filter(geosite_id=extra_info.geosite.id).filter(order__lt=order_selected).order_by("order").last()
    else:
        swap = SiteExtraInfo.objects.filter(geosite_id=extra_info.geosite.id).filter(order__gt=order_selected).order_by("order").first()
    if swap:
        swap_order(selected, swap)
    else:
        logger.error(f"User ({request.user}) encountered error while ordering extra_info for geosite ({extra_info.geosite.name})")
        messages.error(request, "Ordering is in a bad state, this issue has been logged. Please delete the object and try again.")

    return redirect("geosite_edit", geosite_id=extra_info.geosite.id)


@staff_member_required(login_url="/accounts/login")
def switch_order_photographs(request, id, direction):
    photograph = get_object_or_404(SitePhotograph.objects.all(), id = id)

    if not request.user.is_superuser and photograph not in request.user.photographs.all():
        logger.info(f"User ({request.user}) attempted to order photographs for geosite ({photograph.geosite.name} without permission)")
        raise PermissionDenied("You do not have permission to modify this information")
    if direction not in ["left", "right"]:
        logger.info(f"User ({request.user}) gave bad direction when ordering photographs for geosite ({photograph.geosite.name})")
        raise BadRequest("Invalid direction specified")

    selected = SitePhotograph.objects.get(id=id)
    order_selected = selected.order
    if direction == "left":
        swap = SitePhotograph.objects.filter(geosite_id=photograph.geosite.id).filter(order__lt=order_selected).order_by("order").last()
    else:
        swap = SitePhotograph.objects.filter(geosite_id=photograph.geosite.id).filter(order__gt=order_selected).order_by("order").first()
    if swap:
        swap_order(selected, swap)
    else:
        logger.error(f"User ({request.user}) encountered error while ordering photographs for geosite ({photograph.geosite.name})")
        messages.error(request, "Ordering is in a bad state, this issue has been logged. Please delete the object and try again.")

    return redirect("geosite_edit", geosite_id=photograph.geosite.id)

def sitelist_detail(request, slug):
    sitelist = get_object_or_404(SiteList, url_slug=slug)
    if request.user.id != sitelist.user.id and not sitelist.published and not request.user.is_superuser:
        raise Http404

    site_locations = [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [site.geom_point.x, site.geom_point.y]
            },
            "properties": {
                "name": html.escape(site.name)
            }
        } for site in sitelist.sites.all()
    ]
        
    context = {
        "sitelist": sitelist,
        "user_is_owner": request.user.id == sitelist.user.id,
        "OS_API_KEY": settings.OS_API_KEY,
        "site_locations": json.dumps(site_locations),
    }
    return TemplateResponse(request, "geosites_app/sitelist_detail.html", context)

def sitelist_add_site(request, slug, geosite_id):
    sitelist = get_object_or_404(SiteList, url_slug=slug)
    if request.user.id != sitelist.user.id:
        raise Http404
    geosite = get_object_or_404(Geosite, id=geosite_id)
    sitelist.sites.add(geosite)
    return redirect(request.META.get('HTTP_REFERER'))

def sitelist_remove_site(request, slug, geosite_id):
    sitelist = get_object_or_404(SiteList, url_slug=slug)
    if request.user.id != sitelist.user.id:
        raise Http404
    geosite = get_object_or_404(Geosite, id=geosite_id)
    sitelist.sites.remove(geosite)
    return redirect(request.META.get('HTTP_REFERER'))

def sitelist_edit(request, slug):
    sitelist = get_object_or_404(SiteList, url_slug=slug)
    if request.user.id != sitelist.user.id:
        raise Http404
    
    sitelist_form = None
    if request.method == "POST":
        logger.info(f"User ({request.user}) submitted sitelist editing form ({request.POST})")
        sitelist_form = SiteListForm(request.POST, instance=sitelist, site_url=request.build_absolute_uri("/") + "lists/l/")
        if sitelist_form.is_valid(): 
                sitelist_form.save()
                messages.add_message(request, messages.SUCCESS, "Changes saved")
        else:
            for error in sitelist_form.errors:
                messages.add_message(request, messages.ERROR, "There was a problem saving your changes. Please check your input.")
                logger.warning(f"User ({request.user}) encountered error while editing sitelist ({sitelist}): ({error})")

    context = {
        "sitelist_form": sitelist_form or SiteListForm(instance=sitelist, site_url=request.build_absolute_uri("/") + "lists/l/"),
        "sitelist": sitelist,
        "user": request.user,
    }
    logger.info(f"User ({request.user}) is editing ({sitelist})")
    return render(request, "geosites_app/sitelist_edit.html", context)

def sitelist_new(request):
    if not request.user.is_authenticated:
        raise PermissionDenied()
    
    sitelist_form = None
    if request.method == "POST":
        logger.info(f"User ({request.user}) submitted new sitelist form ({request.POST})")
        sitelist_form = SiteListForm(request.POST, site_url=request.build_absolute_uri("/") + "lists/l/")
        if sitelist_form.is_valid(): 
                new_list = sitelist_form.save(commit=False)
                new_list.user = request.user
                new_list.save()
                messages.add_message(request, messages.SUCCESS, "New list added")
        else:
            for error in sitelist_form.errors:
                messages.add_message(request, messages.ERROR, "There was a problem adding your list. Please check your input.")
                logger.warning(f"User ({request.user}) encountered error while adding new sitelist: ({error})")
        
    context = {
        "sitelist_form": sitelist_form or SiteListForm(site_url=request.build_absolute_uri("/") + "lists/l/"),
        "user": request.user,
    }
    logger.info(f"User ({request.user}) is adding a new sitelist")
    return render(request, "geosites_app/sitelist_new.html", context)

def sitelist_delete(request, slug):
    sitelist = get_object_or_404(SiteList, url_slug=slug)
    if request.user.id != sitelist.user.id:
        raise Http404
    
    sitelist.delete()
    logger.info(f"User ({request.user}) deleted sitelist ({sitelist})")
    messages.add_message(request, messages.SUCCESS, "List deleted")
    return redirect("view_account", user_id=request.user.id)