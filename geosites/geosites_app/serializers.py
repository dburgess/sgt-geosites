from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers
from geosites_app.models import Geosite, SitePhotograph

class GeositeSerializerExtent(GeoFeatureModelSerializer):
    gcrsite_block_name = serializers.CharField(source="gcrsite.block_name.translation")
    gcrsite_parent_block_name = serializers.CharField(source="gcrsite.block_name.parent.translation")
    gcrsite_parent_block_color = serializers.CharField(source="gcrsite.block_name.parent.color")
    gcrsite_volume_title = serializers.CharField(source="gcrsite.block_name.gcr_volume.title", default="")
    gcrsite_volume_number = serializers.CharField(source="gcrsite.block_name.gcr_volume.vol_number", default="")
    gcrsite_volume_url = serializers.CharField(source="gcrsite.block_name.gcr_volume.url", default="")
    gcrsite_status = serializers.CharField(source="gcrsite.status.translation")
    gcrsite_chrono_unit = serializers.CharField(source="gcrsite.chrono_unit.translation")
    class Meta:
        model = Geosite
        exclude = ["geom", "geom_point"]
        geo_field = "geom_extent"
        srid = 27700

class GeositeSerializerPoints(GeoFeatureModelSerializer):
    class Meta:
        fields = ["id", "name", "gcrsite_parent_block_name", "gcrsite_parent_block_color"]
        geo_field = "geom_point"
        srid = 27700
        model = Geosite
    gcrsite_parent_block_name = serializers.CharField(source="gcrsite.block_name.parent.translation")
    gcrsite_parent_block_color = serializers.CharField(source="gcrsite.block_name.parent.color")

class GeositeSerializerPolygons(GeoFeatureModelSerializer):
    class Meta:
        fields = ["id", "name", "gcrsite_parent_block_name", "gcrsite_parent_block_color"]
        geo_field = "geom"
        srid = 27700
        model = Geosite
    gcrsite_parent_block_name = serializers.CharField(source="gcrsite.block_name.parent.translation")
    gcrsite_parent_block_color = serializers.CharField(source="gcrsite.block_name.parent.color")

class SitePhotographSerializer(GeoFeatureModelSerializer):
    geosite_name = serializers.CharField(source="geosite.name")
    class Meta:
        model = SitePhotograph
        geo_field = "camera_location"
        fields = ["id", "file", "type", "caption", "geosite", "geosite_name", "look_direction"]
