from django.contrib.gis import admin
from django.urls import reverse
from django.contrib.admin.models import LogEntry
from django.contrib.admin.models import DELETION
from django.utils.html import escape
from django.utils.safestring import mark_safe

class LogEntryAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
    def has_change_permission(self, request, obj=None):
        return False
    def has_delete_permission(self, request, obj=None):
        return False
    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser
    
    date_hierarchy = 'action_time'

    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'action_flag',
    ]

    def object_link(self, obj):
        if obj.action_flag == DELETION:
          link = escape(obj.object_repr)
        else:
          ct = obj.content_type
          link = '<a href="%s">%s</a>' % (
                      reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                      escape(obj.object_repr),
                  )
        return mark_safe(link)

admin.site.register(LogEntry, LogEntryAdmin)

admin.site.site_header = "Geosites administration"
admin.site.site_title = "SGT Geosites Project"
admin.site.index_title = "Geosites administration"

