from rest_framework import routers
from geosites_app.viewsets import GeositeViewset, SitePhotographViewset

router = routers.DefaultRouter()
router.register(r"geosites", GeositeViewset, basename="geosites")
router.register(r"geositephotos", SitePhotographViewset, basename="geosite_photos")

urlpatterns = router.urls