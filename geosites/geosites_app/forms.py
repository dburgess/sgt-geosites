from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.forms import PointField
from leaflet.forms.widgets import LeafletWidget
from .models import SitePhotograph, Geosite, SiteExtraInfo, SiteExtraInfoType, SitePhotographType, SiteList
from .validators import GridRefValidator
from .utils import convert_to_coordinates, get_mime_type
from .widgets import DatePicker, FormAddonPrefix
from django.contrib.auth.models import User

from crispy_forms.helper import FormHelper
from crispy_bulma.layout import Submit
from crispy_bulma.widgets import FileUploadInput


TEXTAREA_ATTRS = {"rows": 5}

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["first_name", "last_name", "email"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["email"].help_text = "This will not be shown publically."

class GeositeForm(forms.ModelForm):
    class Meta:
        model = Geosite
        fields = ["name", "headline", "summary", "notes"]
    
    name = forms.CharField(required=True, label="Name", max_length=100)
    headline = forms.CharField(required=False, label="Headline", max_length=500, widget=forms.Textarea(TEXTAREA_ATTRS))
    summary = forms.CharField(required=False, label="Site summary", max_length=2000, widget=forms.Textarea(TEXTAREA_ATTRS))
    notes = forms.CharField(required=False, label="Site notes", max_length=2000, widget=forms.Textarea(TEXTAREA_ATTRS), help_text='''
                            A box to save notes for other volunteers. Here you can describe any data issues, record who has visited / edited a site,
                            or anything else! These notes will not be shown publicly on the site.
                            ''')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_action = ""
        self.helper.add_input(Submit("edit_geosite", "Save", css_class="is-success"))

class PhotographForm(forms.ModelForm):
    template_name = "geosites_app/forms/photo_form.html"
    file = forms.FileField(required=True, label="Photo", widget=FileUploadInput, help_text="A JPEG or PNG image, or a PDF document. By uploading a file, you affirm that you control the rights to the image. You agree to licence the image for use under the <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)</a> licence.")
    type = forms.ModelChoiceField(required=True, label="Type of photo", queryset=SitePhotographType.objects.all())
    caption = forms.CharField(required=False, label="Caption", widget=forms.Textarea(TEXTAREA_ATTRS))
    date = forms.DateField(required=False, label="Date taken", widget=DatePicker())
    look_direction = forms.IntegerField(required=False, label="View direction (bearing)")
    camera_location = PointField(required=False, label="Photo location", help_text='''
                                The camera location field will be used to place your image on the map.
                                ''', widget=LeafletWidget())
    camera_location_grid_ref = forms.CharField(required=False, label="Grid reference", help_text="Or provide a grid reference (6, 8, or 10 figures)", validators=[GridRefValidator])

    def save(self, commit=True):
        instance = super(PhotographForm, self).save(commit=False)
        if self.cleaned_data["camera_location_grid_ref"]:
            coords = convert_to_coordinates(self.cleaned_data["camera_location_grid_ref"], offset_to_square_center=True)
            geom = GEOSGeometry(f"POINT( {coords[0]} {coords[1]} )", srid=27700)
            instance.camera_location = geom

        instance.mimetype = get_mime_type(instance.file)

        if commit:
            instance.save()
        return instance
    
    def clean_file(self):
        file = self.cleaned_data["file"]
        print(get_mime_type(file))
        if get_mime_type(file) not in settings.ALLOWED_UPLOAD_MIMETYPES:
            raise ValidationError("Unsupported file type. Please upload a JPEG/PNG image or PDF file.")
        return file

class ExtraInfoForm(forms.ModelForm):
    type = forms.ModelChoiceField(label="Type of information", required=True, queryset=SiteExtraInfoType.objects.all())
    content = forms.CharField(label="Information", required=True, help_text='''
                              The text that will be displayed on the website''', widget=forms.Textarea(TEXTAREA_ATTRS))
    link = forms.URLField(label="Link", required=False, help_text='''
                          The webpage to link to, if needed''')

    def clean(self):
        cleaned_data = super().clean()
        type = cleaned_data.get("type") 
        link = cleaned_data.get("link")

        if not type:
            raise ValidationError({"type": "You must provide a type."})

        if not link and type.code in ["EWISE", "GGRAP", "GSCNC", "SLINK"]:
            raise ValidationError({"link": "You must provide a link."})
        elif link:
            if type.code == "EWISE" and "https://earthwise.bgs.ac.uk/" not in link:
                raise ValidationError({"link": "Not a valid Earthwise URL."})
            elif type.code == "GGRAP" and "https://www.geograph.org.uk/" not in link:
                raise ValidationError({"link": "Not a valid Geograph URL."})
            elif type.code == "GSCNC" and "geoscenic.bgs.ac.uk/asset-bank/" not in link:
                raise ValidationError({"link": "Not a valid GeoScenic URL."})
            elif type.code == "SLINK" and "https://sitelink.nature.scot/site/" not in link:
                raise ValidationError({"link": "Not a valid SiteLink link."})


ExtraInfoFormset = forms.modelformset_factory(SiteExtraInfo, form=ExtraInfoForm, fields=["type", "link", "content"], can_delete=True)
class ExtraInfoFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.template = "geosites_app/forms/extra_info_formset.html"
        self.form_tag = False
        self.disable_csrf = True

PhotographFormset = forms.modelformset_factory(SitePhotograph, form=PhotographForm, fields = ["file", "type", "caption", "date", "camera_location", "camera_location_grid_ref", "look_direction"], can_delete=True)
class PhotographFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.template = "geosites_app/forms/photo_formset.html"
        self.form_tag = False
        self.disable_csrf = True

class SiteListForm(forms.ModelForm):
    
    name = forms.CharField(label = "List name")
    published = forms.BooleanField(required=False, label="Visible to others")
    
    class Meta:
        model = SiteList
        fields = ["name", "description", "url_slug", "published"]

    def __init__(self, *args, **kwargs):
        site_url = kwargs.pop("site_url", None)
        super().__init__(*args, **kwargs)
        self.fields["url_slug"] = forms.CharField(widget=FormAddonPrefix(attrs={"prefix": site_url}), label="List URL")