from django.forms import DateInput, TextInput

class DatePicker(DateInput):
    input_type = "date"
    def format_value(self, value):
        return value.isoformat() if value is not None and hasattr(value, "isoformat") else ""
    
    def __init__(self, *args, **kwargs):
        kwargs["attrs"] = {"class": "input"}
        super().__init__(*args, **kwargs)

class FormAddonPrefix(TextInput):
    template_name = "geosites_app/forms/widgets/form_addon_prefix.html"

    prefix = ""

    def __init__(self, attrs=None):
        if attrs:
            if "prefix" in attrs:
                self.prefix = attrs["prefix"]
        return super().__init__(attrs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context["prefix"] = self.prefix
        return context
