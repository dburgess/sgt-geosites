from django.urls import path, include
from django.contrib import admin
from .views import ListGeosites, GeositeDetail, map, view_account, sitelist_detail, sitelist_add_site, sitelist_remove_site, sitelist_edit, sitelist_new, sitelist_delete, about, geosite_edit, list_recent_changes, switch_order_extra_info, switch_order_photographs

urlpatterns = [
    path("", map, name="map"),
    path("list_geosites/", ListGeosites.as_view(), name="list_geosites"),
    path("geosite_detail/<int:geosite_id>", GeositeDetail.as_view(), name="geosite_detail"),
    path("geosite_detail/", GeositeDetail.as_view(), name="geosite_detail"),
    path("geosite_edit/<int:geosite_id>", geosite_edit, name="geosite_edit"),
    path("api/", include("geosites_app.api")),
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("profile/<user_id>", view_account, name="view_account"),
    path("lists/new", sitelist_new, name="sitelist_new"),
    path("lists/l/<slug>", sitelist_detail, name="sitelist_detail"),
    path("lists/l/<slug>/edit", sitelist_edit, name="sitelist_edit"),
    path("lists/l/<slug>/delete", sitelist_delete, name="sitelist_delete"),
    path("lists/l/<slug>/add/<geosite_id>", sitelist_add_site, name="sitelist_add_site"),
    path("lists/l/<slug>/remove/<geosite_id>", sitelist_remove_site, name="sitelist_remove_site"),
    path("about", about, name="about"),
    path("recent/", list_recent_changes, name="recent"),
    path("utils/extra_info/switch_order/<int:id>/<str:direction>", switch_order_extra_info, name="switch_order_extra_info"),
    path("utils/photographs/switch_order/<int:id>/<str:direction>", switch_order_photographs, name="switch_order_photographs"),
]